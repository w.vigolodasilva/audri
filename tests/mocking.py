'''A number of mocking classes for use in tests'''
# pylint: disable=wrong-import-position
import sys
sys.path.append('src')
from visualiser.vehicles import Vehicle

class Surface():
    '''A mocked pygame.Surface'''

    def __init__(self):
        self.hasDrawn = False

    def blit(self, *args):
        '''Set that something has been drawn'''
        self.hasDrawn = True

class Visualiser():
    '''A mocked visualiser.SimulatorVisualiser'''

    def __init__(self):
        self.car = Vehicle(self)
