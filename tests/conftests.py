import unittest
import gui
from gui import conf
from gui.conf import ConfigFrame
import tkinter.ttk as ttk
import tkinter as tk
from gui.main import MainFrame, MainWindow

from config import GUIConfig, SimulatorConfig

visConf = SimulatorConfig()
class TestConfMethods(unittest.TestCase):

    def test_save(self):
        '''check that values are saved correctly'''
        root = tk.Frame()
        main = MainWindow()
        c = ConfigFrame(root,main)
        c._save
        self.assertEqual(visConf.RandomSeed, c._randomSeed.get())
        self.assertEqual(visConf.OffroadAllowed, c._offroad.get())
        self.assertEqual(visConf.FPS, int(c._fps.get()))
        self.assertEqual(visConf.TickRate, int(c._tickrate.get()))
        self.assertEqual(visConf.ScrollBackground, c._scrollBG.get())

    def test_load(self):
        '''check that values are loaded correctly'''
        root = tk.Frame()
        main = MainWindow()
        c = ConfigFrame(root,main)
        c._load
        self.assertEqual(c._randomSeed.get(),visConf.RandomSeed)
        self.assertEqual(c._offroad.get(),visConf.OffroadAllowed)
        self.assertEqual(int(c._fps.get()),visConf.FPS)
        self.assertEqual(int(c._tickrate.get()),visConf.TickRate)
        self.assertEqual(c._scrollBG.get(),visConf.ScrollBackground)
    #TODO
