import unittest, sys
sys.path.append('src')
import gui
from gui import controls
from gui.controls import LabeledScale
from gui.controls import ToolTip

class TestControlsMethods(unittest.TestCase):

#The following tests are for the LabeledScale class

    def test_update(self):
        '''check that cal is set correctly - check both classes update correctly'''
        #TODO

    def test_set(self):
        '''check that value is set correctly'''
        #TODO

    def test_get(self):
        '''check that correct calue is returned'''
        #TODO

#The following tests are for the ToolTip class

    def test_enter(self):
        #TODO
        pass

    def test_leave(self):
        #TODO
        pass

    def test_schedule(self):
        #TODO
        pass

    def test_unschedule(self):
        #TODO
        pass

    def test_show(self):
        #TODO
        pass

    def test_hide(self):
        #TODO
        pass

    def test_cursorPor(self):
        #TODO
        pass
