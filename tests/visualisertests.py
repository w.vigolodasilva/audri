import unittest
import visualiser
import config
from visualiser import visualiser
from visualiser.visualiser import SimulatorVisualiser
from visualiser.util import Actions, loadSprite
from config import SimulatorConfig
from visualiser.vehicles import Obstacle

class TestVisualiserMethods(unittest.TestCase):

    def test_fps (self):
        '''test that function returns correct fps value'''
        simV = SimulatorVisualiser("1")
        self.assertEqual(simV.fps,SimulatorConfig.FPS)

    def test_fps_setter(self):
        '''test that fps is set to selected values'''
        simV = SimulatorVisualiser("1")
        simV.fps = 60
        self.assertEqual(simV.fps, 60, 'value is same')
        # TODO

    def test_tick(self):
        '''Create testing for this function'''
        #TODO

    def test_doAct(self):
        '''Check method returns on no action - check result of action pause -
        check method updates current lane if moved left or right'''
        simV = SimulatorVisualiser("1")
        self.assertEqual(simV.doAct(Actions.NONE), None, 'value is returned')

        simV.pause = False
        simV.doAct(Actions.PAUSE)
        self.assertEqual(simV.pause, True, 'Pause did not work')
        simV.pause = False

        conf = SimulatorConfig()
        conf.OffroadAllowed = True

        simV.car.lane = 1
        simV.doAct(Actions.LEFT)
        self.assertEqual(simV.car.lane, 0, 'lane is incorrect')

        simV.car.lane = 1
        simV.doAct(Actions.RIGHT)
        self.assertEqual(simV.car.lane, 2, 'lane is incorrect')
        #TODO

    def test_keyPress(self):
        '''check invalid action results in return - check doAct is updated with given value'''
        simV = SimulatorVisualiser("1")
        self.assertEqual(simV.keyPress("f"), None, 'value not null')

    def test_draw(self):
        '''Create testing for this function'''

        #TODO

    def test_stateVector(self):
        '''Check that correct values are returned'''
        #TODO

    def test_togglePause(self):
        '''Check that method changes pause'''
        simV = SimulatorVisualiser("1")
        simV.pause = False
        simV.togglePause()
        self.assertEqual(simV.pause, True, 'pause toggled')

    def test_reset(self):
        '''Check values pause-collision-sessionTime-distanceTravelled-obstacles-lane are set correctly'''
        simV = SimulatorVisualiser("1")
        ob = Obstacle(simV)
        simV._obstacles = [ob]
        simV.collisions = 1
        simV.sessionTime = 1
        simV.distanceTravelled = 1
        simV.reset()
        self.assertEqual(simV.collisions, 0, 'collisions')
        self.assertEqual(simV.sessionTime, 0, 'sessions')
        self.assertEqual(simV.distanceTravelled, 0, 'distance travelled')
        self.assertEqual(simV._obstacles, [], 'obstacles')
