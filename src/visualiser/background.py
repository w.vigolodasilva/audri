'''Controlling the background of the visualiser'''
from config import SimulatorConfig
from .vehicles import Vehicle
from .util import loadSprite

conf = SimulatorConfig()

class BackgroundPiece(Vehicle):
    def __init__(self, game, y):
        '''A piece of the background that moves down the screen to emulate the
        car moving forward

        :param game: The parent
            :class:`~visualiser.visualiser.SimulatorVisualiser`
        :param y: The initial y position of the piece
        '''

        super(BackgroundPiece, self).__init__(game)
        self._sprite = 'background.png'
        self._image = loadSprite('background.png')
        self.rect = self.sprite.get_rect()
        self.speed = conf.CarSpeed
        self.pos.y = y

    @property
    def speed(self):
        '''The virtual forward speed of the background piece, in metres per
        second

        :getter: Get the background piece speed
        :setter: Set the background piece speed in m/s
        '''
        return self._speed

    @speed.setter
    def speed(self, val):
        self._speed = val
        # set velocity relative to driver
        self.velocity.y = val*conf.PixelMetreRatio

    def tick(self, dt):
        '''Called regularly to perform update logic.
        Return False to indicate background piece is entirely off screen and
        should be removed'''
        super().tick(dt)
        return self._game.canvas.get_rect().colliderect(self.rect)
