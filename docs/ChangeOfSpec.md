# AUDRI Model - Change of Specification 16/03/18
## Details
Originially AUDRI was created with the use of a neural network to process training data and return actions. Following a supervisor/sponsor meeting on 16.03.18 where some issues concerning the neural network were discuss it was suggested that other training models be investigated/implemented. One of these being a decision tree. 

## Deliverables 

Implementation of a decision tree in place of the neural network, which can be trained on given data and performs an accurate action to avoid collision.


## Requirements
Coding in place (testing) 

 - First attempt code
  
 - Able to access training data
  
 - Able to access controls for vehicle control 
	
Initial Training data (testing)

 - Vehicle State (StateVector)
    	
 - Actions depend on position in road, and other vehicle positions
	
 - New set of data generated per simulator frame 
	


## Potential Additions/Changes
 - Additional Positions (off road, more lanes)
 
 - Access to training data may change (file locations)
