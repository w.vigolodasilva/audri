.. AUDRI documentation master file, created by
   sphinx-quickstart on Tue Apr 24 19:58:20 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to |project|'s documentation!
=====================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   audri
   config
   data
   gui
   visualiser


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
