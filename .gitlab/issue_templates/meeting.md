/title Group/Supervisor meeting d/m/y
<!-- Change the above title as necessary -->
### Details
<!-- 
  Leave two spaces at the end of each line in this section to avoid line spacing 
  Check the preview tab to ensure the layout is correct
-->
__Date:__ d/m/yy  
__Time:__ XXam/pm  
<!-- Location of meeting - building and room number -->
__Location:__ Computer Science C86  
<!-- 
  Team members and others present mention them with @username, for all team members use @all) 
  Update this with the members that actually attended
-->
__Attendees:__ @all

### Agenda
<!--
  The purpose of the meeting - any topics to discuss or questions to answer
  - Use bullet points like this
-->

### Next steps
<!--
  Necessary actions that have been identified and should be performed
  - Use bullet points like this
  Refer to created tasks with #"Task name"
-->

<!-- 
  Leave these below
  1. Mention everyone
  2. Label as meeting
  3. Move to active board
-->
/cc @all 
/label meeting
/board_move ~active
